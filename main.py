# Para poder importar las librerías que están en esta hoja de código, instalar las siguientes
# pip install pandas
# pip install mlxtend
# pip install requests

import pandas as pd
from mlxtend.frequent_patterns import apriori, association_rules

#pd.options.display.max_columns =None
pd.options.display.max_rows = None


import requests 
import zipfile

# Se carga la base de datos de DENUE
df = pd.read_csv("denue_2022_5yrs_nombre_simp.csv", encoding='utf8') 

# Se eliminan todas las columnas que no se vayan a utilizar o que no nos vayan a dar información relevante para el Market Basket
df.drop(['clee', 'denue_razo', 'per_ocu', 'entidad_cv', 'entidad_no', 'denue_via_','denue_vi_1', 
         'denue_vi_2', 'denue_vi_3', 'denue_vi_4','denue_vi_5','denue_vi_6', 'denue_vi_7','numero_ext',
         'letra_ext', 'edificio', 'edificio_e','numero_int','letra_int', 'asentamien', 
         'asentami_1','tipocencom','nom_cencom','num_local',
         'denue_cp','ageb_cvege','tipounieco','denue_tele','denue_lati','denue_long','correoelec','www','censo_actu','censo_alta','edad_negoc', 'max_per_oc','min_per_oc','denue_alta','sector','subsector','sector_nom','subsector','subsector_'], inplace=True, axis = 1)

df['count'] = 1
df.rename(columns = {'id':'id_local'}, inplace=True)

# Se selecciona la categoría de colonias a la que se le quiere aplicar el Market Basket
# Se debe cambiar este valor por 1, 2, 3, 4, 5 y 6 y volver a correr el programa para obtener las reglas de cada categoría
df = df[df["tema"]==1]

print(df.head(10))

# Se genera la matriz
def matriz(dataframe, id=False):
    if id:
    #Agrupamos el DF con las variables que representan los productos, las descomprimimos y reemplazamos los valores nulos con 0
        return dataframe.groupby(['manzana_cv', 'nomb_sim1'])['count'].sum().unstack().fillna(0) .\
        applymap(lambda x: 1 if x > 0 else 0)
  
    else:
        return dataframe.groupby(['manzana_cv', 'nomb_sim1'])['count'].sum().unstack().fillna(0) .\
        applymap(lambda x: 1 if x > 0 else 0)

locales_en_manzanas = matriz(df, id=True)

print(locales_en_manzanas.head(10))

# Se toma el primer registro de la matríz para explorar que actividades estan en esa manzana
print(df.loc[df['manzana_cv']=='1900100010021007'])

# Muestra las actividades en Binario
print(locales_en_manzanas.loc['1900100010021007'].head(10))

# Aplicando el algoritmo Apriori al dataframe previamene codificado con el método One-Hot
locales_elegidos = apriori(locales_en_manzanas, min_support=0.01, use_colnames = True)

#locales_elegidos.sort_values(by=('support'), ascending=False).head()
print(locales_elegidos.head(10))

# Obteniendo las reglas de asociación con la metrica Lift mayor igual a 1.2
reglas_asociacion_lift1 = association_rules(locales_elegidos, metric='lift', min_threshold=1.2)
print(reglas_asociacion_lift1.sort_values(by='lift', ascending=False).head(10))

# Se filtran las reglas de asociacón que solo tienen un consecuente (este consecuente sería el tipo de negocio que le corresponde al antecedente)
reglas_asociacion_lift1_1_consecuente = reglas_asociacion_lift1[reglas_asociacion_lift1["consequents"].str.len()==1]


#print(reglas_asociacion_lift1_1_consecuente["antecedents"].count())


print("________________________________________________")
print(reglas_asociacion_lift1_1_consecuente.sample(10)) #

print("________________________________________________")

# Se filtran las reglas de asociación con un valor de confianza mayor igual a 0.75
reglas_asociacion_conf_lift = reglas_asociacion_lift1_1_consecuente[reglas_asociacion_lift1_1_consecuente['confidence']>0.75]


#
if(reglas_asociacion_conf_lift["antecedents"].count() > 0):
    print("Se encontraron registros con confianza mayores o iguales a 0.75. Exportando archivo con a csv...")
    if(reglas_asociacion_conf_lift["antecedents"].count() > 10):
        print(reglas_asociacion_conf_lift.head(10))
    else:
        print(reglas_asociacion_conf_lift)
    reglas_asociacion_conf_lift.to_csv('output_mkb_col_t_conf_mayor_a_0.75.csv') 
else:
    print("No se encontraron registros con confianza mayores o iguales a 0.75. Exportando archivo a csv...")
    if(reglas_asociacion_lift1_1_consecuente["antecedents"].count() > 10):
            print(reglas_asociacion_lift1_1_consecuente.head(10))
    else:
        print(reglas_asociacion_lift1_1_consecuente)
    reglas_asociacion_lift1_1_consecuente.to_csv('output_mkb_col_t_conf_menor_a_0.75.csv')    





